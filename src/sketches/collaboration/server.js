export default class Collaboration {

    constructor() {
        this.name = this.constructor.name;
    }

    onAddPlayer(playerState) {
        console.log("onAddPlayer");
    }

    onDeletePlayer(playerState) {
        console.log("onDeletePlayer");
    }

    onUpdatePlayer(updates) {
        console.log("onUpdatePlayer");
    }

    onUpdateGlobal(updates) {
        console.log("onUpdateGlobal");
    }
}
