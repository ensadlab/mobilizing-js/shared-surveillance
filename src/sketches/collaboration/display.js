import * as Mobilizing from "@mobilizing/library";

export default class Collaboration {

    constructor() {
        this.name = this.constructor.name;
    }

    preLoad() {

    }

    setup() {

        //access rootNade camera with :
        //this.context.camera.transform.setLocalPositionZ(100);

        this.context.renderer.setCurrentScene(this.name = this.constructor.name);

        this.brickWidth = 1;
        this.meanBrick = new Mobilizing.three.Box({
            /*   "width": this.brickWidth * 5,
              "height": this.brickHeight * 5,
              "depth": (this.brickWidth * 5) / 4, */
            "material": "basic"
        });
        this.meanBrick.transform.setLocalPosition(10, 0, 0);
        //this.meanBrick.material.setWireframe(true);
        this.context.renderer.addToCurrentScene(this.meanBrick);
    }

    update() {

    }

}
