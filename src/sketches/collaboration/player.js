import * as Mobilizing from "@mobilizing/library";

export default class Collaboration {

    constructor() {
        this.name = this.constructor.name;
    }

    preLoad(loader) {
        this.fontRequest = loader.loadArrayBuffer({ "url": "./fonts/Raleway-Regular.ttf" });
        console.log("fontRequest", this.fontRequest);
    }

    setup() {
        //access rootNade camera with :
        //this.context.camera.transform.setLocalPositionZ(500);

        let font = this.fontRequest.getValue();
        this.txt = new Mobilizing.three.TextOpentype({
            fontFile: font,
            text: this.name,
            depth: 0,
            material: "basic",
            lineMode: false
        });

        console.log(this.txt);

        this.context.renderer.addToCurrentScene(this.txt);
        this.txt.material.setWireframe(true);
        this.txt.transform.setLocalPosition(-2, 0, -10);

        this.txtRot = 0;
    }

    update() {
        this.txtRot++;
        this.txt.transform.setLocalRotationY(this.txtRot);
    }

}
