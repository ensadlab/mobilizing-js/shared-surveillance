import * as Mobilizing from "@mobilizing/library";

export default class Confrontation {

    constructor(){
        this.name = this.constructor.name;
    }

    preLoad(loader){
        this.fontRequest = loader.loadArrayBuffer({ "url": "./fonts/Raleway-Regular.ttf" });
        console.log(this.fontRequest);
    }

    setup(){

        let font = this.fontRequest.getValue();
        this.txt = new Mobilizing.three.TextOpentype({
            fontFile: font,
            text: this.name,
            depth: 1,
            material: "basic",
            lineMode: true
        });

        console.log(this.txt);

        this.context.renderer.addToCurrentScene(this.txt);
        this.txt.material.setWireframe(true);
        this.txt.transform.setLocalPosition(-2, 0, -10);

        this.txtRot = 0;
    }

    update(){
        this.txtRot--;
        this.txt.transform.setLocalRotationY(this.txtRot);
    }

}

