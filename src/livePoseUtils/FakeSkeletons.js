export class FakeSkeletons {

    constructor() {
        this.obj = {};
        this.obj.skeletons = {};
        this.skeletonsArray = [];
    }

    addSkeleton(skeleton) {

        let isDoubled = false;

        //avoid double objects
        this.skeletonsArray.forEach((el) => {

            const areEqual = (
                el.camIndex === skeleton.camIndex &&
                el.poseIndex === skeleton.poseIndex);

            if (areEqual) {
                isDoubled = true;
            }
        });

        if (!isDoubled) {

            const skeletonIndex = this.skeletonsArray.indexOf(skeleton);
            if (skeletonIndex >= 0) {
                this.skeletonsArray[skeletonIndex] = skeleton;
            }
            else {
                this.skeletonsArray.push(skeleton);
            }

            console.log("this.skeletonsArray", this.skeletonsArray);

            this.generateMainObject();
        }
    }

    deleteSkeleton(index) {
        this.skeletonsArray.splice(index, 1);

        console.log("this.skeletonsArray", this.skeletonsArray);
        //reste main object
        this.obj.skeletons = {};

        this.generateMainObject();
    }

    animate() {
        this.skeletonsArray.forEach((skeleton) => {
            skeleton.animate();
        });
        this.generateMainObject();
    }

    generateMainObject() {

        this.skeletonsArray.forEach((skeleton) => {

            const camIndex = skeleton.camIndex;
            const poseIndex = skeleton.poseIndex;

            //on a déjà la caméra
            if (this.obj.skeletons[camIndex]) {

                //mais pas la pose
                if (!this.obj.skeletons[camIndex][poseIndex]) {

                    this.obj.skeletons[camIndex][poseIndex] = {};

                    const json = JSON.parse(skeleton.getJSON());
                    this.obj.skeletons[camIndex][poseIndex] = json;
                }
                //on a déjà la pose, on met à jour
                else {
                    const json = JSON.parse(skeleton.getJSON());
                    this.obj.skeletons[camIndex][poseIndex] = json;
                }
            }
            //on a pas déjà la caméra -> faut tout construire
            else {

                this.obj.skeletons[camIndex] = {};

                if (!this.obj.skeletons[camIndex][poseIndex]) {

                    this.obj.skeletons[camIndex][poseIndex] = {};

                    const json = JSON.parse(skeleton.getJSON());
                    this.obj.skeletons[camIndex][poseIndex] = json;
                }
            }
        });
    }

    getJSON() {
        return JSON.stringify(this.obj);
    }

}
