import * as math from '@mobilizing/library/dist/Mobilizing/core/util/Math';

const KEY_POINTS_NAMES = [
    /* "NOSE",
    "NECK",
    "RIGHT_SHOULDER",
    "RIGHT_ELBOW",
    "RIGHT_WRIST",
    "LEFT_SHOULDER",
    "LEFT_ELBOW",
    "LEFT_WRIST",
    "MID_HIP",
    "RIGHT_HIP",
    "RIGHT_KNEE",
    "RIGHT_ANKLE",
    "LEFT_HIP",
    "LEFT_KNEE",
    "LEFT_ANKLE",
    "RIGHT_EYE",
    "LEFT_EYE",
    "RIGHT_EAR",
    "LEFT_EAR",
    "LEFT_BIG_TOE",
    "LEFT_SMALL_TOE",
    "LEFT_HEEL",
    "RIGHT_BIG_TOE",
    "RIGHT_SMALL_TOE",
    "RIGHT_HEEL", */
    //hand
    "PALM", "THUMB_1", "THUMB_2", "THUMB_3", "THUMB_4", "INDEX_FINGER_1", "INDEX_FINGER_2", "INDEX_FINGER_3", "INDEX_FINGER_4", "MIDDLE_FINGER_1", "MIDDLE_FINGER_2", "MIDDLE_FINGER_3", "MIDDLE_FINGER_4", "RING_FINGER_1", "RING_FINGER_2", "RING_FINGER_3", "RING_FINGER_4", "BABY_FINGER_1", "BABY_FINGER_2", "BABY_FINGER_3", "BABY_FINGER_4"
]

export class FakeSkeleton {

    constructor({
        camIndex = 0,
        poseIndex = 0,
        pose_id = 1,
    } = {}) {
        this.camIndex = camIndex;
        this.poseIndex = poseIndex;

        this.pose_id = pose_id;

        this.keypoints = {};

        KEY_POINTS_NAMES.forEach((key) => {

            const x = math.randomFromTo(-100, 100);
            const y = math.randomFromTo(-100, 100);
            const confidence = math.randomFromTo(0, 1);

            this.keypoints[key] = [x, y, confidence];
        });
    }

    animate() {

        KEY_POINTS_NAMES.forEach((key) => {
            this.keypoints[key][0] += math.randomFromTo(-1, 1);
            this.keypoints[key][1] += math.randomFromTo(-1, 1);
            this.keypoints[key][2] += math.randomFromTo(-.5, .5);
        });
    }

    getJSON() {
        const json = {};
        json.pose_id = this.pose_id;
        json.keypoints = this.keypoints;

        return JSON.stringify(json);
    }
}
