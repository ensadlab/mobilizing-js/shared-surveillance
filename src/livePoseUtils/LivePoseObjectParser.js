/* hands keypoints
"PALM", "THUMB_1", "THUMB_2", "THUMB_3", "THUMB_4", "INDEX_FINGER_1", "INDEX_FINGER_2", "INDEX_FINGER_3", "INDEX_FINGER_4", "MIDDLE_FINGER_1", "MIDDLE_FINGER_2", "MIDDLE_FINGER_3", "MIDDLE_FINGER_4", "RING_FINGER_1", "RING_FINGER_2", "RING_FINGER_3", "RING_FINGER_4", "BABY_FINGER_1", "BABY_FINGER_2", "BABY_FINGER_3", "BABY_FINGER_4"
*/

import { Skeleton } from './Skeleton';

const _STATICS = {};

export class LivePoseObjectParser {

    static parseLivePoseStream(livePoseStreamString) {

        //console.log("livePoseStreamString",livePoseStreamString);

        let livePoseData = null;
        //we can have some empty messages (??)
        if (livePoseStreamString) {
            livePoseData = JSON.parse(livePoseStreamString);
        }
        else if (livePoseStreamString === undefined || livePoseStreamString === null) {
            return;
        }

        //const mainType = Object.keys(livePoseData)[0];//seulement un type à la fois
        const mainType = "skeletons";//seulement un type à la fois
        //console.log("mainType",mainType);

        const camerasID = Object.keys(livePoseData[mainType]);
        //console.log("camerasID", camerasID);
        //plusieurs caméras sont présentes
        const cameras = {};

        camerasID.forEach((id) => {
            cameras[id] = livePoseData[mainType][id];
        });

        _STATICS.livePoseData = livePoseData;
        _STATICS.mainType = mainType;
        _STATICS.cameras = cameras;

        //console.log("_STATICS", _STATICS);
    }

    static getMainType() {
        return _STATICS.mainType;
    }

    static getCameras() {
        return _STATICS.cameras;
    }

    static getPoses(camIndex) {

        const cameras = _STATICS.cameras;
        const posesIndex = Object.keys(cameras[camIndex]);

        const poses = {};

        posesIndex.forEach((poseIndex) => {
            poses[poseIndex] = cameras[camIndex][poseIndex];
        });

        return poses;
    }

    static getSkeletons() {

        const skeletons = new Set();

        const cameras = _STATICS.cameras;
        //console.log("cameras", cameras);
        const camerasKeys = Object.keys(cameras);

        camerasKeys.forEach((camIndex) => {
            //console.log("camIndex : ", camIndex);

            const poses = LivePoseObjectParser.getPoses(camIndex);
            const posesKeys = Object.keys(poses);

            posesKeys.forEach((poseIndex) => {

                const skeleton = new Skeleton({
                    camIndex,
                    poseIndex,
                    skeleton: poses[poseIndex]
                });

                if (skeleton.skeleton.keypoints) {
                    skeletons.add(skeleton);
                }

            });
        });

        return skeletons;
    }
}
