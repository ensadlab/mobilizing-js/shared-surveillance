export class Skeleton{

    constructor({
        camIndex = null,
        poseIndex = null,
        skeleton = null,
    } = {}) {
        this.camIndex = camIndex;
        this.poseIndex = poseIndex;
        this.skeleton = skeleton;
    }
}
