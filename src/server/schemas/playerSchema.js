export const playerSchema = {
    id: {
        type: 'integer',
        default: null,
        nullable: true,
    },

    index: {
        type: 'integer',
        default: null,
        nullable: true,
    },

    type:{
        type: "string", //player | display
        default: "player",
    }

};

export default playerSchema;
