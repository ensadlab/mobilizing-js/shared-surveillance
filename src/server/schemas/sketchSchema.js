export const sketchSchema = {

    name: {
        type: "string",
        default: null,
    },

    scripts: {
        type: "any",//array des script à importer
        default: [],
    },

    active: {
        type: "boolean",
        default: false,
    },
};

export default sketchSchema;
