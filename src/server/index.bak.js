/* eslint-disable guard-for-in */
import 'regenerator-runtime/runtime';
import 'source-map-support/register';

import serveStatic from 'serve-static';

import { Server } from '@mobilizing/platform/server/Server.js';
import globalSchema from './schemas/globalSchema.js';
import playerSchema from './schemas/playerSchema.js';
import livePoseSchema from './schemas/livePoseSchema.js';

import { createServer } from 'https';
import { readFileSync } from 'fs';
import { WebSocketServer } from 'ws';

import { LivePoseObjectParser } from '../livePoseUtils/LivePoseObjectParser';
import { Skeleton } from '../livePoseUtils/Skeleton';

const server = new Server();

// avoid server crash
process.on('unhandledRejection', (reason, p) => {
    console.log('> Unhandled Promise Rejection');
    console.log(reason);
});

// function to allow for async
(async function launch() {
    try {
        // -------------------------------------------------------------------
        // launch application
        // -------------------------------------------------------------------
        await server.init();
        await server.start();

        //register schema
        server.stateManager.registerSchema('global', globalSchema);
        server.stateManager.registerSchema('player', playerSchema);
        server.stateManager.registerSchema('livePose', livePoseSchema);

        // single instance of global schema, created here
        const globalState = await server.stateManager.create('global');

        //========== 
        //WebSocket
        //==========
        const WSServer = createServer({
            cert: readFileSync(server.configuration.env.httpsInfos.cert),
            key: readFileSync(server.configuration.env.httpsInfos.key)
        });
        const webSocketPort = server.configuration.env.websockets.handsSocketsPort;

        const webSocketServer = new WebSocketServer({
            server: WSServer
        });

        //const handsStates = new Map();
        const skeletons = new Set();
        const skeletonsStates = new Set();

        webSocketServer.on('connection', (ws) => {

            ws.on('message', (data) => {
                console.log('received: %s', data, "\n");

                const jsonData = data.toString();
                LivePoseObjectParser.parseLivePoseStream(jsonData);

                const skeletonsFromStream = LivePoseObjectParser.getSkeletons();
                //console.log("skeletonsFromStream", skeletonsFromStream, "\n");

                //first shot : add everything
                if (skeletons.size === 0) {

                    skeletonsFromStream.forEach(async (skeletonFromStreamItem) => {
                        //flag for deleting on next frame is needed
                        skeletonFromStreamItem.toDelete = false;
                        skeletons.add(skeletonFromStreamItem);

                        const skeletonState = await server.stateManager.create('livePose', {
                            camIndex: skeletonFromStreamItem.camIndex,
                            poseIndex: skeletonFromStreamItem.poseIndex,
                            skeleton: skeletonFromStreamItem.skeleton
                        });
                        skeletonsStates.add(skeletonState);

                        //setTimeout(() => {
                            console.log("skeletonState", skeletonState.get("camIndex"));
                        //}, 100);
                    });
                }

                //not the first shot, delete or add :
                else {
                    //search for deleting needs
                    skeletons.forEach((skeletonItem) => {

                        skeletonItem.toDelete = true;

                        skeletonsFromStream.forEach((skeletonFromStreamItem) => {

                            const skeletonAreEquals = (
                                skeletonItem.camIndex === skeletonFromStreamItem.camIndex &&
                                skeletonItem.poseIndex === skeletonFromStreamItem.poseIndex
                            );

                            if (skeletonAreEquals) {
                                skeletonItem.toDelete = false;
                            }
                        });
                    });

                    console.log("local skeletons", skeletons, "\n");

                    //then delete it if found
                    skeletons.forEach((skeletonItem) => {
                        if (skeletonItem.toDelete) {
                            skeletons.delete(skeletonItem);
                        }
                    });

                    //flag incoming obj
                    skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                        skeletonFromStreamItem.toAdd = true;
                    });

                    //search incoming object for skeleton to add
                    //AND update the skeletons values for existing ones
                    skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                        skeletons.forEach((skeletonItem) => {

                            const skeletonAreEquals = (
                                skeletonItem.camIndex === skeletonFromStreamItem.camIndex &&
                                skeletonItem.poseIndex === skeletonFromStreamItem.poseIndex
                            );

                            if (skeletonAreEquals) {
                                skeletonFromStreamItem.toAdd = false;
                                //!!update skeleton value!!
                                skeletonItem.skeleton = skeletonFromStreamItem.skeleton;
                            }
                        });
                    });

                    //then add them if found
                    skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                        if (skeletonFromStreamItem.toAdd) {
                            skeletons.add(skeletonFromStreamItem);
                        }
                    });
                }

                console.log("local skeletons", skeletons, "\n");

                /* const hands = JSON.parse(data);
                const handsStatesArray = Array.from(handsStates.values());
                //retire ce qui n'est plus présent dans les données entrantes (LivePos)
                handsStatesArray.forEach((handState) => {
                    console.log("handState",handState);
                    const id = handState.get("id");
                    if (!hands.some((hand) => id === hand.id)) {
                        handsStates.delete(id);
                        server.stateManager.delete(handState);
                    }
                });

                //ajoute ou met à jour les objet entrants
                hands.forEach(async (hand) => {
                    const { id, position } = hand;
                    const state = handsStates.has((s) => id === s.get("id"));
                    if ( !state ) {
                        // create a hand state
                        const handState = await server.stateManager.create('hand', {
                            id,
                            position
                        });
                        handsStates.set(id, handState);
                    }
                    else {
                        state.set({ id, position });
                    }
                });

                console.log("%s", handsStates); */
            });
        });

        WSServer.listen(webSocketPort);


        const playerStates = new Set();

        const combineStates = () => {
            let progressionCount = 0;
            let progressionSum = 0;
            let progressionMin = Infinity;
            let progressionMax = -Infinity;

            playerStates.forEach((state) => {
                // active players
                if (state.get('colour') !== null
                    && state.get('status') === 'running') {
                    ++progressionCount;
                    const progression = state.get('progression');
                    progressionSum += progression;
                    progressionMin = Math.min(progressionMin, progression);
                    progressionMax = Math.max(progressionMax, progression);
                }
            });

            // need at least 2 players
            const progressionMean = (progressionCount > 1
                ? progressionSum / progressionCount
                : 0);
            const progressionSpread
                = (progressionCount > 1
                    ? Math.max(0, Math.min(1, progressionMax - progressionMin))
                    : 0);

            globalState.set({
                progressionMean,
                progressionSpread,
            });

            let playEnding = globalState.get('playEnding');
            // no way back
            if (!playEnding
                && progressionCount > 1
                && progressionMean > 0.95
                && progressionSpread < 0.05) {
                playEnding = true;
                globalState.set({ playEnding });
            }
        };

        const colours = ['bleu', 'jaune'];
        let colourId = -1;
        const prepare = (playerState) => {
            for (let step = 0; step < colours.length;) {
                // try with next index
                ++colourId;
                colourId %= colours.length;
                const colour = colours[colourId];

                // not already assigned
                if (![...playerStates].some((state) => {
                    return state.get('colour') === colour;
                })) {
                    playerState.set({
                        colour,
                        prepare: colour,
                    });
                    break;
                }

                ++step;

                // no unique index
                if (step === colours.length) {
                    // assign something anyway
                    playerState.set({
                        colour,
                        prepare: colour,
                    });
                }
            }

        };

        const preLoad = (playerState) => {
            // nothing before preLoad

            playerState.set({
                preLoad: true,
            });
        };

        // new client
        server.stateManager.observe(async (schemaName, stateId, nodeId) => {
            switch (schemaName) {
                case 'player': {
                    // new player client is created client-side
                    // attach server-side
                    const playerState = await server.stateManager.attach(schemaName, stateId);

                    // keep a set of all players
                    playerStates.add(playerState);

                    const playEnding = false;
                    globalState.set({ playEnding });

                    playerState.subscribe((updates) => {
                        const { status } = updates;
                        if (typeof status !== 'undefined') {

                            switch (status) {
                                case 'waitingForPrepare':
                                    prepare(playerState);
                                    break;

                                case 'waitingForPreLoad':
                                    preLoad(playerState);
                                    break;

                                default:
                                    break;
                            }

                        }

                        // on any change
                        combineStates();
                    }); // udpdates

                    // logic to do when the state is deleted
                    // (e.g. when the player disconnects)
                    playerState.onDetach(() => {
                        // clean things
                        playerStates.delete(playerState);

                        const playEnding = false;
                        globalState.set({ playEnding });

                        // update on new player (which may not have found Eugénie, yet)
                        combineStates();
                    });

                    // update on new player (which should not have found Eugénie, yet)
                    combineStates();

                    break;
                } // 'player'

                default:
                    break;

            }
        });

        // previously created server-side
        globalState.subscribe((updates) => {
            // nothing here
        });

    } catch (err) {
        console.error(err.stack);
    }
})();

