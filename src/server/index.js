/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
import 'regenerator-runtime/runtime';
import 'source-map-support/register';

import { Server } from '@mobilizing/platform/server/Server.js';

import globalSchema from './schemas/globalSchema.js';
import playerSchema from './schemas/playerSchema.js';
// import livePoseSchema from './schemas/livePoseSchema.js';
import sketchSchema from './schemas/sketchSchema.js';

// source files
const sketchesFolderPath = "src/sketches";

import { readdir } from 'node:fs/promises';
import path from 'node:path';

import { createServer } from 'https';
import { readFileSync } from 'fs';
// import { WebSocketServer } from 'ws';
// import { LivePoseObjectParser } from '../livePoseUtils/LivePoseObjectParser';

const server = new Server();

// avoid server crash
process.on('unhandledRejection', (reason, p) => {
    console.log('> Unhandled Promise Rejection');
    console.log(reason);
});

// function to allow for async
(async function launch() {
    try {
        // -------------------
        // launch application
        // -------------------
        await server.init();
        await server.start();

        /*  const sketchManager = new sketchManager();
         const sketches = await sketchManager.importServerSketches();
 
         sketches.forEach((sketchScript, sketchName) => {
             console.log("adding", sketchScript);
             const sketchInstance = new sketchScript();
             sketches.set(sketchName, sketchInstance);
         });
         console.log(sketches); */

        //register schema
        server.stateManager.registerSchema('global', globalSchema);
        server.stateManager.registerSchema('player', playerSchema);
        // server.stateManager.registerSchema('livePose', livePoseSchema);
        server.stateManager.registerSchema('sketch', sketchSchema);
        const sketchesStatesSet = new Set();

        //import sketches from folder
        try {
            const folders = await readdir(sketchesFolderPath, { withFileTypes: true });
            for (const folder of folders) {
                if (folder.isDirectory()) {
                    const files = await readdir(
                        path.join(sketchesFolderPath, folder.name),
                        { withFileTypes: true });

                    const filteredFiles = files.filter((file) => {
                        const isFile = file.isFile();
                        const isJS = path.extname(file.name).toLowerCase() === ".js";
                        return isFile && isJS;
                    });
                    if(filteredFiles.length === 0) {
                        continue;
                    }

                    const scripts = filteredFiles.map((file) => {
                        const { name } = path.parse(file.name);
                        return name;
                    });
                    const sketchState = await server.stateManager.create('sketch', {
                        scripts,
                        name: folder.name,
                    });
                    sketchesStatesSet.add(sketchState);
                }
            }
        } catch (err) {
            console.error(err);
        }

        //console.log(sketchesStatesSet);

        // single instance of global schema, created here
        const globalState = await server.stateManager.create('global');
        /* 
                // -------------------
                // WebSocket for LivePose
                // -------------------
                //create a new server with credential files
                const WSServer = createServer({
                    cert: readFileSync(server.configuration.env.httpsInfos.cert),
                    key: readFileSync(server.configuration.env.httpsInfos.key)
                });
                //change the port if needed in ./config/env/default.json
                const webSocketPort = server.configuration.env.websockets.LivePoseWebSocketPort;
        
                const webSocketServer = new WebSocketServer({
                    server: WSServer
                });
        
                //Dict to handle States from LivePose incoming JSON
                const skeletonsStates = new Set();
        
                webSocketServer.on('connection', (ws) => {
        
                    ws.on('message', async (data) => {
                        //console.log('received: %s', data, "\n");
        
                        if(data === {} || data === "{}" || data === undefined || data === null){
                            return;
                        }
        
                        const jsonData = data.toString();
                      
                        //convert raw data to object
                        LivePoseObjectParser.parseLivePoseStream(jsonData);
        
                        const skeletonsFromStream = LivePoseObjectParser.getSkeletons();
                        //console.log("skeletonsFromStream", skeletonsFromStream, "\n");
        
                        //first shot : add everything
                        if (skeletonsStates.size === 0) {
        
                            //skeletonsFromStream.forEach( async (skeletonFromStreamItem) => {
                            for (const skeletonFromStreamItem of skeletonsFromStream) {
                                // console.log("first connection");
                                // console.log("skeletonFromStreamItem", skeletonFromStreamItem);
        
                                //flag for deleting on next frame is needed
                                skeletonFromStreamItem.toDelete = false;
                                //skeletons.add(skeletonFromStreamItem);
        
                                const skeletonState = await server.stateManager.create('livePose', {
                                    camIndex: skeletonFromStreamItem.camIndex,
                                    poseIndex: skeletonFromStreamItem.poseIndex,
                                    skeleton: skeletonFromStreamItem.skeleton
                                });
        
                                skeletonsStates.add(skeletonState);
                                //console.log("skeletonState", skeletonState);
                            }
                        }
                        //);
        
                        //not the first shot, delete or add :
                        else {
                            //search for deleting needs
                            skeletonsStates.forEach((skeletonItem) => {
        
                                skeletonItem.toDelete = true;
        
                                skeletonsFromStream.forEach((skeletonFromStreamItem) => {
        
                                    const skeletonAreEquals = (
                                        skeletonItem.get("camIndex") === skeletonFromStreamItem.camIndex &&
                                        skeletonItem.get("poseIndex") === skeletonFromStreamItem.poseIndex
                                    );
        
                                    if (skeletonAreEquals) {
                                        skeletonItem.toDelete = false;
                                    }
                                });
                            });
        
                            //console.log(skeletonsStates);
        
                            //then delete it if found
                            skeletonsStates.forEach((skeletonItem) => {
                                if (skeletonItem.toDelete) {
                                    skeletonsStates.delete(skeletonItem);
                                    //delete from stateManager to mimic a disconnection
                                    skeletonItem.delete();
                                }
                            });
        
                            //flag incoming obj
                            skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                                skeletonFromStreamItem.toAdd = true;
                            });
        
                            //search incoming object for skeleton to add
                            //AND update the skeletons values for existing ones
                            skeletonsFromStream.forEach((skeletonFromStreamItem) => {
                                skeletonsStates.forEach((skeletonItem) => {
        
                                    const skeletonAreEquals = (
                                        skeletonItem.get("camIndex") === skeletonFromStreamItem.camIndex &&
                                        skeletonItem.get("poseIndex") === skeletonFromStreamItem.poseIndex
                                    );
        
                                    if (skeletonAreEquals) {
                                        skeletonFromStreamItem.toAdd = false;
                                        //!!update skeleton value!!
                                        skeletonItem.set({ skeleton: skeletonFromStreamItem.skeleton });
                                    }
                                });
                            });
        
                            //then add them if found
                            //skeletonsFromStream.forEach(async (skeletonFromStreamItem) => {
                            for (const skeletonFromStreamItem of skeletonsFromStream) {
        
                                if (skeletonFromStreamItem.toAdd) {
        
                                    const skeletonState = await server.stateManager.create('livePose', {
                                        camIndex: skeletonFromStreamItem.camIndex,
                                        poseIndex: skeletonFromStreamItem.poseIndex,
                                        skeleton: skeletonFromStreamItem.skeleton
                                    });
                                    skeletonsStates.add(skeletonState);
                                }
                            }
                            //});
                        }
        
                        // const debugSkeletonsStates = Array.from(skeletonsStates.values());
                        // debugSkeletonsStates.map( (state, index) => {
                        //     console.log("index : ", index, state.getValues());
                        // });
        
                    });
                });
        
                WSServer.listen(webSocketPort); */

        //playerStates
        const playerStates = new Set();

        const preLoad = (playerState) => {
            // nothing before preLoad

            playerState.set({
                preLoad: true,
            });
        };

        // new client
        server.stateManager.observe(async (schemaName, stateId, nodeId) => {
            switch (schemaName) {
                case 'player': {
                    // new player client is created client-side
                    // attach server-side
                    const playerState = await server.stateManager.attach(schemaName, stateId);

                    // keep a set of all players
                    playerStates.add(playerState);
                   
                    //nb of player in list
                    /* let playerCount = 0;
                    playerStates.forEach((playerState) => {
                        console.log("current player index : ", playerCount);
                        playerState.set({ index: playerCount });
                        playerCount++;
                    }); */

                    playerState.subscribe((updates) => {
                       
                    }); // udpdates

                    // logic to do when the state is deleted
                    // (e.g. when the player disconnects)
                    playerState.onDetach(() => {
                        // clean things
                       
                        playerStates.delete(playerState);
                    });

                    break;
                } // 'player'

                default:
                    break;

            }
        });

        // previously created server-side
        globalState.subscribe((updates) => {
            // nothing here
        });

    }
    catch (err) {
        console.error(err.stack);
    }
})();

