import { Client } from '@mobilizing/platform/client/Client.js';
import { render, html } from 'lit-html';
import { FakeSkeleton } from '../../livePoseUtils/FakeSkeleton';
import { FakeSkeletons } from '../../livePoseUtils/FakeSkeletons';
import { handDatas } from "./livePoseData";

import { trt_hand_15hz } from "./trt_hand_15hz";
import { trt_hand_30hz_confidence_90 } from "./trt_hand_30hz_confidence_90";
import { trt_hand_30hz } from "./trt_hand_30hz";
import { trt_hand_60hz } from "./trt_hand_60hz";
import { trt_hand_15Hz_6cams_3D } from "./trt_hand_15Hz_6cams_3D";
import { livepose_5hz_6cam } from "./livepose_5hz_6cam"

import * as math from '@mobilizing/library/dist/Mobilizing/core/util/Math';

class LivePoseSimulatorClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });

        this.client = this;
        this.container = container;
        this.rafId = null;

        this.poses = new Map();
        this.poseLastID = 0;

        this.fakeSkeletons = new FakeSkeletons();

        /* this.skeletonsData = trt_hand_15hz;
        this.animateFPS = 1000 /15;*/

        /* this.skeletonsData = trt_hand_30hz_confidence_90;
        this.animateFPS = 1000 / 30;*/

        /* this.skeletonsData = trt_hand_30hz;
        this.animateFPS = 1000 / 30; */

        /*  this.skeletonsData = trt_hand_60hz;
         this.animateFPS = 1000 / 60; */

        /* this.skeletonsData =  trt_hand_15Hz_6cams_3D;
        this.animateFPS = 1000 / 5; */

        this.skeletonsData = livepose_5hz_6cam;
        this.animateFPS = 1000 / 5;

        this.skeletonsDataCurrentIndex = 0;
        console.log(this.skeletonsData);
    }

    async start() {
        await super.start();

        //WS settings from global state
        this.globalState = await this.client.stateManager.attach('global');

        const webSocketPort = this.client.configuration.env.websockets.LivePoseWebSocketPort;
        console.log("webSocketPort =", webSocketPort);

        try {
            this.webSocket = new WebSocket(`wss://${window.location.hostname}:${webSocketPort}`);
        }
        catch (error) {
            console.error("webSocket Error : ", error);
        }

        window.addEventListener('resize', () => this.render());
        this.render();

        //fake animation of values
        //setInterval(() => this.animate(), this.animateFPS);

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');
        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }
        });
    }

    sendSkeletonFromData() {
        //if (this.skeletonsDataCanPlay) {
        this.skeletonToSend = this.skeletonsData[this.skeletonsDataCurrentIndex];
        this.webSocket.send(JSON.stringify(this.skeletonToSend));
        console.log("skeletonToSend", this.skeletonToSend);
        this.skeletonsDataCurrentIndex++;
        this.skeletonsDataCurrentIndex %= this.skeletonsData.length;
        //}

    }

    sendSkeletons() {
        const json = this.fakeSkeletons.getJSON();
        //WS send
        this.webSocket.send(json);
    }

    //pour faire un test de modification des positions
    animate() {
        this.fakeSkeletons.animate();
        this.sendSkeletons();
    }

    render() {
        document.body.style.backgroundColor = "gray";
        // debounce with requestAnimationFrame
        window.cancelAnimationFrame(this.rafId);

        //const hands = Array.from(this.hands.values());

        //   <button
        //    @click="${(e) => {
        //         const newSkeleton = new FakeSkeleton();
        //         newSkeleton.camIndex = Math.floor(math.randomFromTo(0, 6));
        //         newSkeleton.poseIndex = Math.floor(math.randomFromTo(0, 6));
        //         this.fakeSkeletons.addSkeleton(newSkeleton);
        //         //this.sendSkeletons();
        //     }}"
        //   >Add Skeleton</button>

        //   <button
        //   @click="${(e) => {
        //         const idToDelete = math.randomFromTo(0, this.fakeSkeletons.skeletonsArray.length);
        //         this.fakeSkeletons.deleteSkeleton(idToDelete);
        //         console.log(this.fakeSkeletons);
        //         //this.sendSkeletons();
        //     }}"
        //  >Delete Skeleton</button>

        // <button
        //   @click="${(e) => {
        //         this.sendSkeletons();
        //     }}"
        //  >Send Skeleton</button>

        render(html`
      <div class="controller">
        <p>
        
         <button
           @click="${(e) => {
                this.skeletonsDataPlayingInterval = setInterval(() => this.sendSkeletonFromData(), this.animateFPS);
            }}"
          >Play Skeleton Data</button>

          <button
          @click="${(e) => {
                clearInterval(this.skeletonsDataPlayingInterval);
            }}"
         >Stop Skeleton Data</button>

         <input name="hz" id="hz" type="range" min="0" value="10"
         @input="${(e) => {
                this.animateFPS = 1000 / e.target.value;
                clearInterval(this.skeletonsDataPlayingInterval);
                this.skeletonsDataPlayingInterval = setInterval(() => this.sendSkeletonFromData(), this.animateFPS);
            }}">
        </p>


        <pre class="handsDisplay"><code>
        hands: "${JSON.stringify(this.skeletonToSend, null, 2)}"
        </code></pre>
      </div>
    `, this.container);

        this.rafId = window.requestAnimationFrame(() => this.render());
    }
}

export default LivePoseSimulatorClient;
