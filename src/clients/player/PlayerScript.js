import * as Mobilizing from '@mobilizing/library';

export class PlayerScript {

    constructor({
        context,
        reportCallback,
    } = {}) {
        this.context = context;
        this.reportCallback = reportCallback;
        this.needsUserInteraction = true;

        this.throttleFreq = 1000 / 30;

        this.prepare();
    }

    async prepare() {
        //give access to all the registered sketches
       /*  this.sketches = await this.sketchManager.importPlayerSketches();

        this.sketches.forEach((sketchScript, sketchName) => {
            //console.log("adding", sketchScript);
            this.context.addScript(sketchScript);
            //console.log("added", this.context.scripts[this.context.scripts.length-1]);
            this.sketches.set(sketchName, this.context.scripts[this.context.scripts.length-1]);
        }); */
        //console.log(this.sketches);
    }

    preLoad(loader) {
        // const urlPrefix = "./assets/audio/";
    }

    setup() {
        this.context.renderer.setCurrentScene("default");

        this.context.camera.transform.setLocalPositionZ(10);

        this.world = new Mobilizing.three.Node();
        this.context.renderer.addToCurrentScene(this.world);

        //construct rootNode relationship
        this.sketches.forEach((sketchScript, sketchName) => {
            console.log(this.context.renderer.scenes);
            this.world.transform.addChild(this.context.renderer.scenes[sketchName].transform);
        });
    }

    update() { }

    apply(/* {} = {} */) {

    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }

        // direct binding
        this.reportCallback(updates);
    }
}
