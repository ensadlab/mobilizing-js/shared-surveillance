
import * as Mobilizing from '@mobilizing/library';
import { Client } from '@mobilizing/platform/client/Client.js';
import { PlayerScript } from './PlayerScript';

export class PlayerClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });
        this.client = this;
        this.container = container;

        this.context = new Mobilizing.MultiScriptContext({
            renderer: true
        });

        this.playerScript = new PlayerScript({
            context: this.context,
            reportCallback: (updates) => this.report(updates), // bind this
        });
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        // move soundworks container under
        this.container.style.zIndex = -100;
        this.container.style.position = 'absolute';

        //maintain a Map of all players
        this.playerStates = new Map();

        // create a player state
        this.playerState = await this.client.stateManager.create('player', {
            id: this.client.id,
            type: "player", //WARNING : use display for display
        });

        //Sketches
        this.sketchesStates = new Set();

        // register to sketches
        this.sketchesState = await this.client.stateManager.attach('sketch');
        this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {
            switch (schemaName) {
                case "sketch":
                    const sketchState = await this.client.stateManager.attach(schemaName, stateId);
                    this.sketchesStates.add(sketchState);

                    const clientType = this.playerState.get("type");
                    const { name, scripts } = sketchState.getValues();

                    if (scripts.some((script) => clientType === script)) {

                        // start of path must be static (relative)
                        // for webpack to handle it
                        const imported = await import(
                            `../../sketches/${name}/${clientType}.js`
                        );
                        console.log('imported script',
                                    `${name}/${clientType}`,
                                    imported);
                    }

                    break;
            }
        });

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');

        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }

            /*  const { skeletonsPositions } = updates;
 
             if (skeletonsPositions) {
                 //console.log(skeletonsPositions);
                 const currentPlayerIndex = this.playerState.get("index");
                 if (currentPlayerIndex !== null) {
                     const skeletonsPositionsForCamera = skeletonsPositions[currentPlayerIndex];
                     this.playerScript.applySkeletonsPositions(skeletonsPositionsForCamera);
                 }
             } */

            // direct binding
            //this.playerScript.apply(updates);
        });

        //check the script for the need of user interaction
        if (this.playerScript.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }

        //wait for mobilizing context to be prepared
        this.context.events.on("setupDone", () => {
            console.log("!setupDone!");

            //listen to others players
            this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {
                switch (schemaName) {

                    case "player": {

                        const state = await this.client.stateManager.attach(schemaName, stateId);
                        //console.log(state);

                        //player disappears
                        state.onDetach(() => {
                            //this.playerScript.removePlayer(stateId);
                            this.playerStates.delete(stateId);
                            this.playerScript.applyRemoveTouch(stateId);
                        });

                        //subscribe to updates coming from shared states
                        state.subscribe((updates) => {
                            const point = state.get("touch");
                            this.playerScript.applyUdpateTouch(stateId, point);
                            //console.log(stateId, touch);
                        });

                        //add the player state to the Map
                        this.playerStates.set(stateId, state);
                        //this.playerScript.applyMakeNewTouch(stateId);

                        break;
                    }

                    case "livePose": {
                        // volatile livePose instance may already be gone...
                        try {
                            const state = await this.client.stateManager.attach(schemaName, stateId);

                            //skeletons disappears
                            state.onDetach(() => {
                                this.playerScript.removeSkeleton(stateId);
                            });

                            //adds new skeleton to the main map
                            this.playerScript.addSkeleton(stateId, state);

                        } catch (error) {
                            // console.log(`+++++++++++++ Error while attaching livePose ${stateId}`, error.message);
                        }
                        break;
                    }
                }
            });
        });

        this.context.addComponent(this.playerScript);
        /*  //adds imported scripts if any
         if (this.playerScript.sketches.length > 0) {
             this.playerScript.sketches.forEach((sketch) => {
                 this.context.addScript();
             });
         } */

        this.runner = new Mobilizing.Runner({
            context: this.context,
            autoPrepare: false,
        });

    }

    report(updates) {
        // direct binding
        //console.log("updates", updates);
        this.playerState.set(updates);
    }

}

export default PlayerClient;
