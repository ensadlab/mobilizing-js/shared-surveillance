import * as Mobilizing from '@mobilizing/library';
import { Client } from '@mobilizing/platform/client/Client.js';
import { DisplayScript } from './DisplayScript.js';

export class DisplayClient extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });
        this.client = this;
        this.container = container;

        this.context = new Mobilizing.MultiScriptContext({
            renderer: true
        });

        this.displayScript = new DisplayScript({
            context: this.context,
            reportCallback: (updates) => this.report(updates), // bind this
        });

        this.playerStates = new Map();
    }

    async init() {
        await super.init();
        // features may be available
    }

    async start() {
        await super.start();
        // available features should be ready

        // move soundworks container under
        this.container.style.zIndex = -100;
        this.container.style.position = 'absolute';

        // register to the already existing global state
        this.globalState = await this.client.stateManager.attach('global');

        //subscribe to state updates
        this.globalState.subscribe((updates) => {
            const { reload } = updates;
            if (reload) {
                window.location = window.location; // eslint-disable-line
            }

            //generic binding
            this.displayScript.apply(updates);

        });

        this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {

            switch (schemaName) {

                case "player": {

                    const state = await this.client.stateManager.attach(schemaName, stateId);
                    //console.log(state);

                    //player disappears
                    state.onDetach(() => {
                        this.displayScript.removePlayer(stateId);
                        this.playerStates.delete(stateId);
                    });
                    //subscribe to updates coming from shared states
                    state.subscribe((updates) => {
                        //console.log(updates);
                        this.displayScript.updatePlayer(stateId, updates, state);

                        if (this.playerStates.size > 0) {
                        }
                    });
                    //adds new skeleton to the main map
                    //this.displayScript.addPlayer(stateId, state);

                    this.playerStates.set(stateId, state);

                    break;
                }

                case "livePose": {

                    // volatile livePose instance may already be gone...
                    try {
                        const state = await this.client.stateManager.attach(schemaName, stateId);
                        console.log("state", state);
                        //skeletons disappears
                        state.onDetach(() => {
                            this.displayScript.removeSkeleton(stateId);
                        });

                        //subscribe to updates coming from shared states
                        state.subscribe((updates) => {
                            this.displayScript.updateSkeleton(stateId, updates);
                        });

                        //adds new skeleton to the main map
                        this.displayScript.addSkeleton(stateId, state);
                    } catch (error) {
                        // console.log(`+++++++++++++ Error while attaching livePose ${stateId}`, error.message);
                    }
                    break;
                }

                default:
                    break;
            }
        });

        //check the script for the need of user interaction
        if (this.displayScript.needsUserInteraction) {
            this.context.userInteractionDone.setup();
            await this.context.userInteractionDone.promise;
        }

        this.context.addComponent(this.displayScript);

        this.context.events.on("setupDone", () => {
            console.log("setupDone");/* 
            const showSkeletonsPoints = this.globalState.get("showSkeletonsPoints");
            const showSkeletonsSegments = this.globalState.get("showSkeletonsSegments");
            const showSkeletonsBBox = this.globalState.get("showSkeletonsBBox");
            //console.log(showPoints,showSegments,showBBox);
            this.displayScript.apply({ showSkeletonsPoints });
            this.displayScript.apply({ showSkeletonsSegments });
            this.displayScript.apply({ showSkeletonsBBox }); */
        });

        this.runner = new Mobilizing.Runner({
            context: this.context,
            autoPrepare: false,
        });
    }

    //for data and events coming from Mobilizing user script
    report(updates) {

        const { skeletonsPositions } = updates;
        //console.log(skeletonsPositions);
        this.globalState.set({ skeletonsPositions });
    }

}

export default DisplayClient;
