import * as Mobilizing from '@mobilizing/library';
// import { SketchManager } from '../../sketches/SketchManager';

export class DisplayScript {

    constructor({
        context,
        reportCallback,
    } = {}) {
        this.context = context;
        this.reportCallback = reportCallback;
        //this.needsUserInteraction = true;

        /* this.showSkeletonsPoints = true;
        this.showSkeletonsSegments = true;
        this.showSkeletonsBBox = true; */

        this.cameraZ = 10;
        this.currentCameraZ = this.cameraZ;
        this.cameraZMax = 1200;
        this.cameraZMin = 1;

        // this.sketchManager = new SketchManager();
        //manual prepare to have preLoad chain effective
        this.prepare();
    }

    async prepare() {
        //give access to all the registered sketches
        // this.sketches = await this.sketchManager.importDisplaySketches();

        this.sketches.forEach((sketchScript, sketchName) => {
            //console.log("adding", sketchScript);
            this.context.addScript(sketchScript);
            //console.log("added", this.context.scripts[this.context.scripts.length-1]);
            this.sketches.set(sketchName, this.context.scripts[this.context.scripts.length - 1]);
        });
        console.log(this.sketches);
    }

    preLoad(loader) {
        console.log("display main load");
    }

    setup() {
        this.context.renderer.setCurrentScene("default");

        this.context.camera.transform.setLocalPositionZ(this.cameraZ);
        console.log(this.context, this.context.camera);
        this.context.camera.setNearPlane(0.01);
        //this.context.renderer.setClearColor(Mobilizing.three.Color.red);
        //audio
        /*  this.audioRenderer = new Mobilizing.audio.Renderer();
         this.context.addComponent(this.audioRenderer);
         this.audioRenderer.setup();
  */
        //lights
        //this.context.light.transform.setLocalPosition(100, 100, 10);

        const touch = this.context.touch;
        touch.setup();//set it up
        touch.on();//active it

        const mouse = this.context.mouse;
        mouse.setup();//set it up
        mouse.on();//active it

        mouse.events.on("mousewheel", (x, y) => {
            console.log(y);
            this.currentCameraZ += y / 10;

            if (this.currentCameraZ < this.cameraZMin) {
                this.currentCameraZ = this.cameraZMin;
            }
            if (this.currentCameraZ > this.cameraZMax) {
                this.currentCameraZ = this.cameraZMax;
            }
            this.context.camera.transform.setLocalPositionZ(this.currentCameraZ);
        });

        this.pointer = this.context.pointer;
        this.pointer.add(touch);
        this.pointer.add(mouse);

        this.world = new Mobilizing.three.Node();
        this.context.renderer.addToCurrentScene(this.world);

        //construct rootNode relationship
        this.sketches.forEach((sketchScript, sketchName) => {
            console.log(this.context.renderer.scenes);
            this.world.transform.addChild(this.context.renderer.scenes[sketchName].transform);
        });

        this.makeWorldOrigin();

        //hide it if no player is connected
        //this.meanBrick.setVisible(false);

        //this.context.light.transform.setLocalPosition(0, 100, 0);
        /*  const light2 = new Mobilizing.three.Light();
         light2.transform.setLocalPosition(-100, -100, 0);
         this.context.renderer.addToCurrentScene(light2); */
    }

    makeWorldOrigin() {

        //simple helpers for world navigation
        this.worldOrigin = new Mobilizing.three.Node();
        const worldOriginLength = 5;
        const worldX = new Mobilizing.three.Line({
            point1: new Mobilizing.three.Vector3(),
            point2: new Mobilizing.three.Vector3(worldOriginLength, 0, 0),
            material: "basic",
        });
        worldX.material.setColor(Mobilizing.three.Color.red.clone());

        const worldY = new Mobilizing.three.Line({
            point1: new Mobilizing.three.Vector3(),
            point2: new Mobilizing.three.Vector3(0, worldOriginLength, 0),
            material: "basic",
        });
        worldY.material.setColor(Mobilizing.three.Color.green.clone());

        const worldZ = new Mobilizing.three.Line({
            point1: new Mobilizing.three.Vector3(),
            point2: new Mobilizing.three.Vector3(0, 0, worldOriginLength),
            material: "basic",
        });
        worldZ.material.setColor(Mobilizing.three.Color.blue.clone());

        this.worldOrigin.transform.addChild(worldX.transform);
        this.worldOrigin.transform.addChild(worldY.transform);
        this.worldOrigin.transform.addChild(worldZ.transform);

        this.world.transform.addChild(this.worldOrigin.transform);
    }

    update() {

        if (this.pointer.getState()) {
            this.world.transform.setLocalRotationX(this.world.transform.getLocalRotationX() + this.pointer.getDeltaY() / 5);
            this.world.transform.setLocalRotationY(this.world.transform.getLocalRotationY() + this.pointer.getDeltaX() / 5);
        }
    }

    /*=============
    *  Players (mobile)
    * =============
    */
    removePlayer(stateId) {
        const brick = this.bricks.get(stateId);

        //free the slots used by brick in grid
        const xIndex = brick.xIndex;
        const yIndex = brick.yIndex;

        this.bricksSlots[xIndex][yIndex] = false;

        //this.context.renderer.removeFromCurrentScene(brick);
        this.world.transform.removeChild(brick.transform);
        this.bricks.delete(stateId);

        if (this.bricks.size === 0) {
            this.meanBrick.setVisible(false);
        }

        //hands accumulator
        this.recordingHands.delete(stateId);
    }

    updatePlayer(stateId, updates, state) {
        //console.log(updates);
        const { acc, quaternion, touch } = updates;
        const brick = this.bricks.get(stateId);
        /* brick.transform.setLocalRotationOrder("ZXY");
        brick.transform.setLocalRotation(acc.z, -acc.x, -acc.y + 180); */
        if (quaternion) {
            const gyroQuat = new Mobilizing.three.Quaternion(
                quaternion.x,
                quaternion.y,
                quaternion.z,
                quaternion.w
            );
            brick.transform.setLocalQuaternion(gyroQuat);
        }

        if (touch) {
            const index = state.get("index");
            //console.log(index, touch);
            const obj = { index, touch };
            this.recordingHands.set(stateId, obj);
        }
    }

    addPlayer(stateId, state) {

        //manage grid base organisation with an abstract list of available slots
        let xIndex = null;
        let yIndex = null;
        let shouldBreak = false;

        for (let i = 0; i < this.bricksMaxLines; i++) {
            for (let j = 0; j < this.bricksMaxColumns; j++) {

                if (!this.bricksSlots[i][j]) {
                    this.bricksSlots[i][j] = true;
                    xIndex = i;
                    yIndex = j;
                    shouldBreak = true;
                    break;
                }
            }
            if (shouldBreak) {
                break;
            }
        }
        //console.log("xIndex", xIndex, "yIndex", yIndex, this.bricksSlots);

        const brick = new Mobilizing.three.Box({
            "width": this.brickWidth,
            "height": this.brickHeight,
            "depth": this.brickWidth / 4
        });

        brick.xIndex = xIndex;
        brick.yIndex = yIndex;

        const x = this.bricksStartPosition.x + yIndex * this.brickStep;
        const y = this.bricksStartPosition.y - xIndex * this.brickStep * 1.2;
        //console.log("x", x, "y", y);

        brick.transform.setLocalPosition(x, y, 0);
        this.context.renderer.addToCurrentScene(brick);
        this.world.transform.addChild(brick.transform);

        this.bricks.set(stateId, brick);

        this.currentBrickIndex++;

        if (this.bricks.size > 0) {
            this.meanBrick.setVisible(true);
        }
    }

    updateMeanBrick(updates) {

        const { meanAcc, meanQuat } = updates;

        if (meanAcc) {
            /*  this.meanBrick.transform.setLocalRotationOrder("ZXY");
             this.meanBrick.transform.setLocalRotation(meanAcc.z, -meanAcc.x, -meanAcc.y + 180); */
        }
        else if (meanQuat) {
            this.meanBrick.transform.setLocalQuaternion(meanQuat);
        }
    }

    /*
    * =============
    *  LivePose Skeletons
    * =============
    */

    removeSkeleton(stateId) {
        const root = this.skeletons.get(stateId);
        //this.context.renderer.removeFromCurrentScene(root);
        this.world.transform.removeChild(root.transform);
        //TODO : destroy mesh to clear memory
        this.skeletons.delete(stateId);
    }

    addSkeleton(stateId, state) {

        const stateValues = state.getValues();

        const camIndex = stateValues.camIndex;
        const poseIndex = stateValues.poseIndex;
        const skeleton = stateValues.skeleton;
        const pose_id = skeleton.pose_id;

        //flat keypoints array
        const tempKeyPoints = Object.values(skeleton.keypoints);
        console.log("gestures", skeleton.gesture);

        const keyPoints = [];
        tempKeyPoints.forEach((keyPoint) => {
            //we can have 2D coords or 3D!
            let point = null;

            if (keyPoint.length === 3) { //2D
                point = new Mobilizing.three.Vector3(Number(keyPoint[0]), Number(keyPoint[1]), 0);
            }
            else if (keyPoint.length === 4) { //3D

                /* const pointIsOrigin = Number(keyPoint[0]) === 0 && Number(keyPoint[1]) === 0 && Number(keyPoint[2]) === 0;
                 if (pointIsOrigin) {
                     console.log("camIndex", camIndex, "poseIndex", poseIndex, "pose_id", pose_id, "skeleton", skeleton);
                 } */
                point = new Mobilizing.three.Vector3(
                    Number(keyPoint[0]),
                    Number(keyPoint[2]),
                    -Number(keyPoint[1]));
            }
            keyPoints.push(point);
        });

        const root = new Mobilizing.three.Node();
        root.camIndex = Number(camIndex);
        root.poseIndex = Number(poseIndex);
        root.pose_id = pose_id;
        root.stateId = stateId;

        //console.log("keyPoints", keyPoints);
        const mesh = new Mobilizing.three.Segments({
            points: keyPoints
        });
        mesh.setVisible(this.showSkeletonsSegments);
        //#0
        root.transform.addChild(mesh.transform);

        const pointCloud = new Mobilizing.three.Points({
            points: keyPoints
        });
        pointCloud.setPointsSize(1);
        //console.log("this.showSkeletonsPoints",this.showSkeletonsPoints);
        pointCloud.setVisible(this.showSkeletonsPoints);
        //#1
        root.transform.addChild(pointCloud.transform);

        const bbox = pointCloud.getSize();
        const center = pointCloud.getCenter();
        const box = new Mobilizing.three.Box({
            width: bbox.x,
            height: bbox.y,
            depth: bbox.z,
            material: "basic"
        });
        box.transform.setLocalPosition(center.x, center.y, center.z);
        box.material.setWireframe(true);
        box.material.setTransparent(true);
        box.material.setOpacity(this.bboxOpacity);

        box.material.setColor(Mobilizing.three.Color.red);
        box.setVisible(this.showSkeletonsBBox);
        //#2
        //this.context.renderer.addToCurrentScene(box);
        root.transform.addChild(box.transform);

        root.transform.setLocalScale(100);

        //this.context.renderer.addToCurrentScene(root);
        this.world.transform.addChild(root.transform);

        //add the new skeleton to the storage Map
        this.skeletons.set(stateId, root);
        //console.log(this.skeletons);

        this.updateInteraction(root);
    }

    cloneSkeleton(baseRoot) {

        const root = new Mobilizing.three.Node();
        root.camIndex = baseRoot.camIndex;
        root.poseIndex = baseRoot.poseIndex;
        root.pose_id = baseRoot.pose_id;

        const pointMesh = baseRoot.transform.getChild(0);
        const points = pointMesh.getPoints();
        //console.log("points", points);
        const mesh = new Mobilizing.three.Segments({
            points: points
        });
        mesh.setVisible(this.showSkeletonsSegments);
        //#0
        root.transform.addChild(mesh.transform);

        const pointCloud = new Mobilizing.three.Points({
            points: points
        });
        pointCloud.setPointsSize(1);
        //console.log("this.showSkeletonsPoints",this.showSkeletonsPoints);
        pointCloud.setVisible(this.showSkeletonsPoints);
        //#1
        root.transform.addChild(pointCloud.transform);

        const bbox = pointCloud.getSize();
        const center = pointCloud.getCenter();
        const box = new Mobilizing.three.Box({
            width: bbox.x,
            height: bbox.y,
            depth: bbox.z,
            material: "basic"
        });
        box.transform.setLocalPosition(center.x, center.y, center.z);
        box.material.setWireframe(true);
        box.material.setTransparent(true);
        box.material.setOpacity(this.bboxOpacity);

        box.material.setColor(Mobilizing.three.Color.red);
        box.setVisible(this.showSkeletonsBBox);
        //#2
        //this.context.renderer.addToCurrentScene(box);
        root.transform.addChild(box.transform);

        root.transform.setLocalScale(100);

        //this.context.renderer.addToCurrentScene(root);
        this.world.transform.addChild(root.transform);

        //add the new skeleton to the storage Map
        this.skeletons.set(baseRoot.stateId + Math.floor(Mobilizing.math.randomFromTo(0, 1000)), root);

        //console.log("clone skeleton");
    }

    updateSkeleton(stateId, updates) {

        const skeleton = updates.skeleton;
        //flat keypoints array
        const tempKeyPoints = Object.values(skeleton.keypoints);
        //console.log("tempKeyPoints", tempKeyPoints);
        const keyPoints = [];
        tempKeyPoints.forEach((keyPoint) => {
            //we can have 2D coords or 3D!
            let point = null;

            if (keyPoint.length === 3) { //2D
                point = new Mobilizing.three.Vector3(Number(keyPoint[0]), -Number(keyPoint[1]), 0);
            }
            else if (keyPoint.length === 4) { //3D
                point = new Mobilizing.three.Vector3(
                    Number(keyPoint[0]),
                    Number(keyPoint[2]),
                    -Number(keyPoint[1]));
            }
            keyPoints.push(point);
        });

        const root = this.skeletons.get(stateId);

        const mesh = root.transform.getChild(0);
        keyPoints.forEach((point, index) => {
            mesh.setPoint(index, point);
        });
        //mesh.setVertices(keyPoints);

        //console.log("updated", mesh.getVertices());

        const pointCloud = root.transform.getChild(1);
        keyPoints.forEach((point, index) => {
            pointCloud.setPoint(index, point);
        });
        //pointCloud.setVertices(keyPoints);

        const bbox = pointCloud.getNativeGeometry();
        bbox.computeBoundingBox();
        const v = new Mobilizing.three.Vector3();
        bbox.boundingBox.getSize(v);
        //console.log("bbox",v);

        let box = root.transform.getChild(2);
        root.transform.removeChild(box.transform);
        box.erase();
        box = null;

        const newBbox = pointCloud.getSize();
        const center = pointCloud.getCenter();
        box = new Mobilizing.three.Box({
            width: newBbox.x,
            height: newBbox.y,
            depth: newBbox.z,
            material: "basic"
        });
        box.material.setColor(Mobilizing.three.Color.red);
        box.transform.setLocalPosition(center.x, center.y, center.z);
        box.material.setWireframe(true);
        box.material.setTransparent(true);
        box.material.setOpacity(this.bboxOpacity);
        box.setVisible(this.showSkeletonsBBox);

        root.transform.addChild(box.transform);

        this.recordingHands.forEach((obj) => {
            const index = obj.index;
            const touch = obj.touch;
            //console.log(index, touch);
            if (root.camIndex === index) {
                if (touch.x !== null) {
                    this.cloneSkeleton(root);
                }
            }
        });

        this.updateInteraction(root);
    }

    //interaction manager
    updateInteraction(root) {

        const camIndex = root.camIndex;
        const poseIndex = root.poseIndex;
        const points = root.transform.getChild(1);
        const bbox = root.transform.getChild(2);

        if (!this.reportSkeletonsPositions[camIndex]) {
            this.reportSkeletonsPositions[camIndex] = {};
        }

        const pos = new Mobilizing.three.Vector3();
        bbox.getNativeObject().getWorldPosition(pos);

        this.reportSkeletonsPositions[camIndex][poseIndex] = { x: pos.x, y: pos.y, z: pos.z };

        if (camIndex === 0) {
            if (poseIndex === 0) {
                const xToColor = Mobilizing.math.map(pos.x, -100, 100, 0, 1);
                const color = new Mobilizing.three.Color(xToColor, 0, 0);
                //console.log(color);
                //this.context.renderer.setClearColor(color);
            }
        }

        this.report({ skeletonsPositions: this.reportSkeletonsPositions });
        //console.log(this.reportSkeletonsPositions);
    }

    //controller
    applyShowSkeletonsPart() {
        this.skeletons.forEach((skeleton) => {

            const points = skeleton.transform.getChild(1);
            const segments = skeleton.transform.getChild(0);
            const bbox = skeleton.transform.getChild(2);

            points.setVisible(this.showSkeletonsPoints);
            segments.setVisible(this.showSkeletonsSegments);
            bbox.setVisible(this.showSkeletonsBBox);
        });
    }

    apply(updates) {

        //console.log(updates);
        const { showSkeletonsPoints, showSkeletonsSegments, showSkeletonsBBox } = updates;

        if (showSkeletonsSegments !== undefined) {
            this.showSkeletonsSegments = showSkeletonsSegments;
            this.applyShowSkeletonsPart();
        }
        if (showSkeletonsPoints !== undefined) {
            this.showSkeletonsPoints = showSkeletonsPoints;
            this.applyShowSkeletonsPart();
        }
        if (showSkeletonsBBox !== undefined) {
            this.showSkeletonsBBox = showSkeletonsBBox;
            this.applyShowSkeletonsPart();
        }
    }

    report(updates) {
        if (!this.reportCallback) {
            return;
        }

        // direct binding
        this.reportCallback(updates);
    }
}
