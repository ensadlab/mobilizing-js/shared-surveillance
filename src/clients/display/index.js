import 'core-js/stable';
import 'regenerator-runtime/runtime';

import { launcher } from '@mobilizing/platform/client/launcher.js';

import DisplayClient from './DisplayClient.js';

async function launchFunction(container, index) {
  try {
    // init user code

    const client = new DisplayClient({
      container,
    })
    await client.init();
    await client.start();

    // promise contains at least a client
    return Promise.resolve({client});
  } catch(err) {
    console.error(err);
    return Promise.reject(err);
  }
}

// -------------------------------------------------------------------
// bootstrapping
// -------------------------------------------------------------------
(async function bootstrap() {
  const container = document.querySelector('#__mobilizing-container');

  const launched = await launcher({
    container,
    launchFunction,
  });

  // for debugging purposes:
  // contains {client, player} for a single client
  // ... or an Array<{client, player}>
  window.app = launched;
})();
