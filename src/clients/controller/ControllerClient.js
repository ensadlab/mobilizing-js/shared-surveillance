/* eslint-disable default-case */
import { Client } from '@mobilizing/platform/client/Client.js';

import { render, html } from 'lit-html';
import { isEvent } from '@mobilizing/platform/shared/schema.js'

class ControllerExperience extends Client {
    constructor({
        features,
        container,
    } = {}) {
        super({
            features,
            container,
        });

        this.client = this;
        this.container = container;
        this.rafId = null;
    }

    async start() {
        await super.start();

        this.playerStates = new Map();
        this.livePoseStates = new Map();

        this.client.stateManager.observe(async (schemaName, stateId, nodeId) => {

            switch (schemaName) {

                case 'player': {
                    const state = await this.client.stateManager.attach(schemaName, stateId);

                    state.onDetach(() => {
                        this.playerStates.delete(stateId);
                        this.render();
                    });
                    state.subscribe(() => this.render());

                    this.playerStates.set(stateId, state);

                    this.render();
                    break;
                }

                case 'livePose': {
                    const state = await this.client.stateManager.attach(schemaName, stateId);

                    state.onDetach(() => {
                        this.livePoseStates.delete(stateId);
                        this.render();
                    });

                    state.subscribe(() => this.render());

                    this.livePoseStates.set(stateId, state);

                    this.render();
                    break;
                }
            }
        });

        console.log();

        this.globalState = await this.client.stateManager.attach('global');
        this.globalState.subscribe((updates) => this.render());

        window.addEventListener('resize', () => this.render());
        this.render();
    }

    render() {
        // debounce with requestAnimationFrame
        window.cancelAnimationFrame(this.rafId);

        const debug = this.globalState.get('debug');
        const showSkeletonsPoints = this.globalState.get('showSkeletonsPoints');
        const showSkeletonsSegments = this.globalState.get('showSkeletonsSegments');
        const showSkeletonsBBox = this.globalState.get('showSkeletonsBBox');

        const players = Array.from(this.playerStates.values());
        const livePose = Array.from(this.livePoseStates.values());

        //pour récupérer uniquement ce qui n'est pas un événement dans les schèmas
        const getNonEventValues = (state) => {
            const schema = state.getSchema();
            const values = state.getValues();
            const nonEventValues = {};
            Object.entries(values).forEach(([key, value]) => {
                if (!isEvent(schema, key)) {
                    Object.assign(nonEventValues, { [key]: value });
                }
            })
            return nonEventValues;
        };

        render(html`
      <div class="controller">
        <p>
          <button
           @click="${e => this.globalState.set({ reload: true })}"
          >Reload all clients</button>
        </p>

        <p>
          <button
           class="${debug ? 'debugToggle' : ''}"
           @click="${e => this.globalState.set({ debug: !debug })}"
          >Debug Display</button>
        </p>

        <p>
        <button
        class="${showSkeletonsPoints ? 'debugToggle' : ''}"
         @click="${e => this.globalState.set({ showSkeletonsPoints: !showSkeletonsPoints })}"
        >Show Skeletons Points</button>

        <button
        class="${showSkeletonsSegments ? 'debugToggle' : ''}"
         @click="${e => this.globalState.set({ showSkeletonsSegments: !showSkeletonsSegments })}"
        >Show Skeletons Segements</button>

        <button
        class="${showSkeletonsBBox ? 'debugToggle' : ''}"
         @click="${e => this.globalState.set({ showSkeletonsBBox: !showSkeletonsBBox })}"
        >Show Skeletons Bounging Box</button>
      </p>

        ${debug ? html`
        <pre class="debugDisplay"><code>
        globals:
        ${JSON.stringify(getNonEventValues(this.globalState), null, 2)}

        players:
        ${players.map((state) => {
            const displayValues = getNonEventValues(state);
            return JSON.stringify(displayValues, null, 2);
        }).join('\n')}

        livePose:
        ${livePose.map((state) => {
            const displayValues = getNonEventValues(state);
            return JSON.stringify(displayValues, null, 2);
        }).join('\n')}

        </code></pre>
      ` : ''}
      </div>
      `, this.container);

        this.rafId = window.requestAnimationFrame(() => this.render());
    }
}

export default ControllerExperience;
