#!/bin/bash

# double-click
cd "$( dirname "$0" )" || (echo "no dir: ${0}"; exit 1)

if [ -f '../secrets/sudo_secrets' ] ; then
    source '../secrets/sudo_secrets'
fi

# get environment variables as arguments
environment_variables=("${@}")
eval "${environment_variables[@]}"

# use PORT if defined, or 8000
: ${PORT:=8000}

runner="$0"
runner_basename="$(basename "$runner")"
runner_path="$(pwd)"
runner_pid=$$
server_port="$PORT"

clean_up() {
    # stop system services
    echo "$sudo_password" | sudo -S -k launchctl unload /Library/Server/Web/Config/Proxy/apache_serviceproxy.conf;
    echo "$sudo_password" | sudo -S -k serviceproxyctl stop
    echo "$sudo_password" | sudo -S -k killall httpd

    # kill other instance of runner on same port, but not self
    echo "$sudo_password" | sudo -S -k bash -c "ps -awwxf | grep -v grep | grep \"$runner_basename\" | grep \"$PORT\" | grep -v \"$runner_pid\" | awk '{print \$2}' | xargs kill"


    # kill node process and sub-processes
    echo "$sudo_password" | sudo -S -k bash -c "ps auwwx | grep -v grep | grep node | grep \"$runner_path\" | awk '{print \$2}' | xargs kill"

    # kill any process on server_port
    process_id_on_same_port=$(echo "$sudo_password" | sudo -S -k lsof -t -i :"$server_port")
    echo process_id_on_same_port "${process_id_on_same_port}"
    echo "$sudo_password" | sudo -S -k kill "$process_id_on_same_port"
}

quit() {
    clean_up
    exit 0
}

trap quit SIGTERM SIGQUIT

if [ ! "$sudo_password" ] ; then
    echo
    echo "########################################"
    echo "# Please type your password            #"
    echo "########################################"
    echo

    read -r -s sudo_password

    sudo -k
    while (( $(echo "$sudo_password" | sudo -S -k echo 1 || echo 0) == 0 )) ; do
        echo
        echo "########################################"
        echo "# Please type your password again      #"
        echo "########################################"
        echo
        read -r -s sudo_password
    done

    echo "OK"
    echo

else
    echo sudo password known
fi

mkdir -p "logs"
while true; do
    log_file="logs/npm_run_$(date +"%Y-%m-%d_%H-%M-%S").log"
    date | tee "$log_file"

    echo | tee -a "$log_file"
    echo '#######' | tee -a "$log_file"
    echo | tee -a "$log_file"
    echo "runner=${runner}" | tee -a "$log_file"
    echo "runner_basename=${runner_basename}" | tee -a "$log_file"
    echo "runner_path=${runner_path}" | tee -a "$log_file"
    echo "runner_pid=${runner_pid}" | tee -a "$log_file"
    echo "server_port=${server_port}" | tee -a "$log_file"
    echo | tee -a "$log_file"
    echo '#######' | tee -a "$log_file"
    echo | tee -a "$log_file"

    clean_up | tee -a "$log_file"

    echo "start with environment: " "${environment_variables[@]}" | tee -a "$log_file"
    echo "$sudo_password" | sudo -S -k "${environment_variables[@]}" npm run dev | tee -a "$log_file"

    date | tee -a "$log_file"
    echo 'ended' | tee -a "$log_file"
    sleep 1 | tee -a "$log_file"
done
